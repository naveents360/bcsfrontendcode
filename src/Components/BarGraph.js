import React from 'react';
import {Bar} from 'react-chartjs-2';
import {MONTHS,YEARS,API_URL} from './Constants'
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';

import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import TableContainer from '@material-ui/core/TableContainer';



export default class App extends React.Component {
    constructor(props){
        super(props);
        this.state={
          months:  [],
          year:'',
      }
      }

      handleyearfield = event=>{
        event.preventDefault()
        this.setState({year: event.target.value},()=>
          this.updatefunc(this.state.year)
          //console.log()
        )
        
      }

      updatefunc(year){
        console.log(year)
        axios.get(API_URL+"getPolicyDetailsByMonth?year="+year)
        .then(res => {
          if(res.status === 200){
          console.log(res.data.msg)
        this.setState({months:res.data.msg})
        }
        })
      .catch(error => {
        if(error.response){
          console.log(error.response)
      }
        })     

      }

      componentDidMount(){
        axios.get(API_URL+"getPolicyDetailsByMonth?year=2021")
        .then(res => {
          if(res.status === 200){
          console.log(res.data.msg)
        this.setState({months:res.data.msg})
        }
        })
      .catch(error => {
        if(error.response){
          console.log(error.response)
      }
        })     
      }
      
  render() {
    const state = {
      labels: MONTHS,
      datasets: [
        {
          label: 'Policy Count',
          backgroundColor: 'rgba(75,192,192,1)',
          borderColor: 'rgba(0,0,0,1)',
          borderWidth: 1,
          data: this.state.months
        }
      ]
    }
    return (
        <TableContainer>
            <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               All Policies 
        </Typography>
        <Divider  fullwidth  />
        <Grid container spacing={2}>
        <Grid item xs={3}>
        <FormControl variant="outlined" margin="normal" 
          fullWidth>
        <InputLabel>Choose Year</InputLabel>
        <Select          
          value={this.state.year}
          onChange={this.handleyearfield}
          label="Search"
        >
          {
            YEARS.map((sf,index)=>
            <MenuItem key={index} value={sf}>{sf}</MenuItem>)
          }
        </Select>
        </FormControl>
        </Grid>
        </Grid>
      <div>

        <Bar
          data={state}
          options={{
            title:{
              display:true,
              text:'No of Policies per Month',
              fontSize:20
            },
            legend:{
              display:true,
              position:'right'
            }
          }}
        />
      </div>
      </TableContainer>
    );
  }
}