import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from  'react-router-dom';
import Container from '@material-ui/core/Container';
//import { super } from '@babel/types';

export default class Home extends React.Component{
    // constructor(props){
    //     super(props);
    // }

    render(){
        return(
            <Container component="main" maxWidth="xs"style={{textAlign:"center"}}>
            <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                   Home Page
            </Typography>
            <Button><Link to="/AllPolicies">All Policies</Link></Button>
            <br />
            <Button><Link to="/bargraph">Graphical Representation</Link></Button>
            </Container>

        )
    }
}
