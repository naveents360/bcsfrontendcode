import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
//import { Link,Redirect } from  'react-router-dom';
//import {API_URL,getToken} from './constants'
import {API_URL,SEARCHFEILDS} from './Constants'
import { Redirect } from  'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import EditIcon from '@material-ui/icons/Edit';
import SearchBar from "material-ui-search-bar";
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';


class AllPolicies extends React.Component{
    constructor(props){
        super(props);
        this.state={
          policies:  [],
          open:false,
          premium:'',
          policyid:'',
          searchfeild:'',
          value:''
      }
      }

      handleClick = parameter => event=>{
        event.preventDefault();
        
        this.state.policies.forEach(element=>{
          if(element.id === parameter){
            this.setState({policyid:element.id})
            this.setState({premium:element.Premium})
          }
          
        })
        this.setState({policyid:parameter})
        this.setState({open:!this.state.open})
        
      }
            
      handlesearchfield = event=>{
        event.preventDefault()
        this.setState({searchfeild: event.target.value})
      }

      handlesearch = ()=>{
       
        if (this.state.searchfeild === "CUSTOMER ID"){
          axios.get(API_URL+"getPolicyDetailsByCustomerId?cust_id="+this.state.value)
          .then(res => {
            if(res.status === 200){
          this.setState({policies:res.data})
          }
          })
        .catch(error => {
          if(error.response){
            console.log(error.response)
        }
          })     
        }
        else{
          axios.get(API_URL+"getPolicyDetailsByIdFilter?policy_id="+this.state.value)
          .then(res => {
            if(res.status === 200){
          this.setState({policies:res.data})
          }
          })
        .catch(error => {
          if(error.response){
            console.log(error.response)
        }
          })     

        }
      }

        componentDidMount() {
         // this.state.projectname=this.props.location.state.projectname
 
          axios.get(API_URL+"getAllPolicyDetails")
            .then(res => {
              if(res.status === 200){
            this.setState({policies:res.data})
            }
            })
          .catch(error => {
            if(error.response){
              console.log(error.response)
          }
            })     
        
        }

        componentWillUnmount(){
          this.setState({open:!this.state.open})
        }

      render() {
        var {policies}=this.state;
        if(this.state.open){
          return <Redirect to={{
            pathname:"/updatepolicy",
            state:{policyid:this.state.policyid,premium:this.state.premium}
          }}
          />
      }
        return (
          <TableContainer>
            <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               All Policies 
        </Typography>
        <Divider  fullwidth  />
        <Grid container spacing={2}>
        <Grid item xs={3}>
        <FormControl variant="outlined" margin="normal" 
          fullWidth>
        <InputLabel>Search Category</InputLabel>
        <Select          
          value={this.state.searchfeild}
          onChange={this.handlesearchfield}
          label="Search"
        >
          {
            SEARCHFEILDS.map((sf,index)=>
            <MenuItem key={index} value={sf}>{sf}</MenuItem>)
          }
        </Select>
        </FormControl>
        </Grid>
      
        <Grid item xs={4}>
        <SearchBar
        placeholder={this.state.searchtext}
    value={this.state.value}
    onChange={(newValue) => this.setState({ value: newValue })}
    onRequestSearch={() => this.handlesearch(this.state.value)}
  />
  
        </Grid>
        
        </Grid>
        <Divider  fullwidth  />
            
            <Table aria-label="customized table">
              <TableHead>
                <TableRow>
                 <TableCell>Sl.No</TableCell>
                  <TableCell>Policy ID</TableCell>
                  <TableCell align="center">Customer ID&nbsp;</TableCell>
                  <TableCell align="center">Fuel&nbsp;</TableCell>
                  <TableCell align="center">Premium&nbsp;</TableCell>
                  <TableCell align="center">Edit Policy Details&nbsp;</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {policies.map((policy, index) => (
                  <TableRow key={policy.id}>
                  <TableCell>{index+1}</TableCell>
                    <TableCell align="center">{policy.id}</TableCell>
                    <TableCell align="center">{policy.customer}</TableCell>
                    <TableCell align="center">{policy.Fuel}</TableCell>
                    <TableCell align="center">{policy.Premium}</TableCell> 
                    <TableCell align="center">
                    <EditIcon fontSize = "small" onClick={this.handleClick(policy.id)}/>
                    </TableCell>               
                      
                  </TableRow>
               
                ))}
              </TableBody>
            </Table>
            
          </TableContainer>
        );
      }
     }
    
export default AllPolicies;