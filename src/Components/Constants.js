export const API_URL = "https://infraportal-api.jio.com:9443/"

export const FUELCHOICES = ["CNG","Petrol","Diesel"]
export const VEHICLESEGMENT = ["A","B","C"]
export const CHOICES = ["0","1"]
export const SEARCHFEILDS = ["POLICY ID","CUSTOMER ID"]
export const MONTHS = ["January","February","March","April","May","June","July","August","September","October","November","December"]
export const YEARS = ["2015","2016","2017","2018","2019","2020","2021"]

export function ValidatePremiumAmount(premium){
    let premiumhelpertext = ''
    if(!premium){
        premiumhelpertext ="Premium Amount Should Not be None"
    }
    premium = parseInt(premium)
    if(isNaN(premium)){
        premiumhelpertext = "Please Enter Only Numbers Between 1 to 1000000"
    }
    if(premium<1 || premium>1000000){
        premiumhelpertext = "Value Should be in between 1 to 1000000"
    }
    return premiumhelpertext
}
