import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
//import {API_URL, getToken,encryptdata,validateipaddress,validateusernamelength,validatepassword,verifyheader} from './constants';
import { Redirect } from  'react-router-dom';
import {API_URL,FUELCHOICES,VEHICLESEGMENT,CHOICES,ValidatePremiumAmount} from './Constants'


export default class UpdatePolicy extends React.Component{
    constructor(props){
        super(props);
        this.state={
        policyid:'',
        customerid:'',
        fuel:'',
        vehiclesegment:'',
        premium:'',
        bodilyinjuryfeild:'',
        personal:'',
        collison:'',
        property:'',
        comprehensive:'',
        premiumhelpertext:'',
        open:false,
        loading:false
        }     
       }

        componentDidMount(){
        
        this.setState({policyid:this.props.location.state.policyid})
        this.setState({premium:this.props.location.state.premium})
        axios.get(API_URL+'getPolicyDetailsById?policy_id='+this.props.location.state.policyid )
 .then(res => {
     // If request is good...
    
     this.setState({premium:res.data.Premium})
     this.setState({customerid:res.data.customer})
     this.setState({personal:res.data.Personal})
     this.setState({fuel:res.data.Fuel})
     this.setState({vehiclesegment:res.data.Vehicle_segment})
     this.setState({bodilyinjuryfeild:res.data.Bodily_injury_liability})
     this.setState({collison:res.data.Collision})
     this.setState({property:res.data.Property})
     this.setState({comprehensive:res.data.Comprehensive})
    //  console.log(response.data.Premium)
    //  this.setState({projects:response.data.message.sort()})
  })
 .catch((error) => {
  if(error.response){
    this.setState({return:!this.state.return})
  }
  });
    }
       handleClose = event =>{
         event.preventDefault();
         this.setState({open:!this.state.open})
       }
       handlebodilyinjuryfeild = event =>{
        event.preventDefault();
        this.setState({bodilyinjuryfeild: event.target.value})
    }

       handlefuel = event =>{
        event.preventDefault();        
        this.setState({fuel: event.target.value})
    }

       handlecollison = event =>{
           event.preventDefault();
           this.setState({collison: event.target.value})
       }
       handlecomprohensive= event =>{
           event.preventDefault();
           this.setState({comprehensive:event.target.value})
       }
       handlevehiclesegment = event=>{          
           event.preventDefault();
           this.setState({vehiclesegment:event.target.value})
           }
        handlecustomerid= event=>{          
            event.preventDefault();
            this.setState({customerid:event.target.value})
            }
        handlepremium= event=>{          
            event.preventDefault();
            this.setState({premium:event.target.value})
            }
            handlepersonal= event=>{          
                event.preventDefault();
                this.setState({vehiclesegment:event.target.value})
                }
                handleproperty= event=>{          
                    event.preventDefault();
                    this.setState({vehiclesegment:event.target.value})
                    }
                    

       
      
        handleClick(event){
        event.preventDefault();
        //this.setState({open: !this.state.open })
        //this.setState({loading: !this.state.loading})
        let premiumhelpertext = ValidatePremiumAmount(this.state.premium)
        if(premiumhelpertext){
            this.setState({premiumhelpertext})
            return
        }
            const payload = {
               "Fuel":this.state.fuel,
               "Vehicle_segment":this.state.vehiclesegment,
               "Premium":this.state.premium,
               "Bodily_injury_liability":this.state.bodilyinjuryfeild,
               "Personal":this.state.personal,
               "Collision":this.state.collison,
               "Property":this.state.property,
               "Comprehensive":this.state.comprehensive,
               "customer":this.state.customerid
           } 
          
          
         
       // const isvalid = true
       
           
          this.setState({loading: !this.state.loading})
        this.setState({open: !this.state.open})
        this.setState({message:''})
        
        axios.post(API_URL+'updatePremiumByPolicyId?policy_id='+this.state.policyid,payload)
        .then(res => {
          this.setState({loading:!this.state.loading})
            if(res.status === 200){
        
            this.setState({message:res.data.msg})           
          }
          else if(res.status === 201){
             
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){            
            this.setState({return:!this.state.return})
        }
          })     
          
        }
        
         
         
        
    render(){
      //const disabled = !this.state.serverip.length || !this.state.env.length || !this.state.password.length || !this.state.project.length || !this.state.username.length;
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
              Edit Policy
        </Typography>
        <form  noValidate>
          <TextField           
            error ={false}
            variant="outlined"
            margin="normal"
            fullWidth
            required
            disabled
            label="Policy ID"
            name="policyid"      
            autoFocus
            onChange = {this.handleChangeserverip}
            helperText = {this.state.iphelptext}  
            value={this.state.policyid}          
          />
          <br/>
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Customer ID"
            name="customerID"     
            onChange = {this.handlecustomerid}
            autoFocus
            helperText ={this.state.userhelptext}
            value = {this.state.customerid}
          />
          <br/>

          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Premium"
            name="Premium"     
            onChange = {this.handlepremium}
            autoFocus
            helperText ={this.state.premiumhelpertext}
            value = {this.state.premium}
          />
          <br/>
            
      <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Fuel</InputLabel>
        <Select          
          value={this.state.fuel}
          onChange={this.handlefuel}
          label="Fuel"
        >
          {
            FUELCHOICES.map((fuel,index)=>
            <MenuItem key={index} value={fuel}>{fuel}</MenuItem>)
          }
        </Select>
      </FormControl>
      <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Vehicle Segment</InputLabel>
        <Select          
          value={this.state.vehiclesegment}
          onChange={this.handlevehiclesegment}
          label="Fuel"
        >
          {
            VEHICLESEGMENT.map((vs,index)=>
            <MenuItem key={index} value={vs}>{vs}</MenuItem>)
          }
        </Select>
      </FormControl>
      <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Bodily Injured Liability</InputLabel>
        <Select          
          value={this.state.bodilyinjuryfeild}
          onChange={this.handlebodilyinjuryfeild}
          label="BIL"
        >
          {
            CHOICES.map((choice,index)=>
            <MenuItem key={index} value={choice}>{choice}</MenuItem>)
          }
        </Select>
      </FormControl>

       <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Personal Injury Protection</InputLabel>
        <Select          
          value={this.state.personal}
          onChange={this.handlepersonal}
          label="BIL"
        >
          {
            CHOICES.map((choice,index)=>
            <MenuItem key={index} value={choice}>{choice}</MenuItem>)
          }
        </Select>
      </FormControl>

      <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Collision</InputLabel>
        <Select          
          value={this.state.collison}
          onChange={this.handlecollison}
          label="BIL"
        >
          {
            CHOICES.map((choice,index)=>
            <MenuItem key={index} value={choice}>{choice}</MenuItem>)
          }
        </Select>
      </FormControl>

       <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Property Damage Liability </InputLabel>
        <Select          
          value={this.state.property}
          onChange={this.handleproperty}
          label="BIL"
        >
          {
            CHOICES.map((choice,index)=>
            <MenuItem key={index} value={choice}>{choice}</MenuItem>)
          }
        </Select>
      </FormControl>

       <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>Comprehensive</InputLabel>
        <Select          
          value={this.state.comprehensive}
          onChange={this.handlecomprohensive}
          label="BIL"
        >
          {
            CHOICES.map((choice,index)=>
            <MenuItem key={index} value={choice}>{choice}</MenuItem>)
          }
        </Select>
      </FormControl>
  
          
          <Button
            //disabled = {disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            Update Policy            
           </Button>                
            <br/>        

            
        <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

