import React from 'react';
import { BrowserRouter as Router, Route,Switch } from 'react-router-dom'; 
import Home from './Components/Home'
import AllPolicies from './Components/AllPolicies'
import UpdatePolicy from './Components/UpdatePolicy'
import { Redirect } from  'react-router-dom';
import BarGraph from './Components/BarGraph'

export default class App extends React.Component{
  render(){
    return(
      <Router>
      <Switch> 
      <Route exact path='/home' component={Home}></Route>
      <Route exact path='/updatepolicy' component={UpdatePolicy}></Route>
      <Route exact path='/AllPolicies' component={AllPolicies}></Route>
      <Route exact path='/bargraph' component={BarGraph}></Route>
      <Route render={() => <Redirect to="/home" />} />

        </Switch>
        </Router>
    )
  }
}
